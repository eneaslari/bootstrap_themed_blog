var mongoose =require('./dbConnection');

var imageSchema = new mongoose.Schema({  type:String,
										data:Buffer,
										postID:String										
									});


var Image = mongoose.model('Image', imageSchema);

module.exports=Image;