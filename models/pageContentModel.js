var mongoose =require('./dbConnection');

var pageContentSchema = new mongoose.Schema({  
                                    content:String,
                                    tag:String,
                                    page:String                                          								
									});


var PageContent = mongoose.model('PageContent', pageContentSchema);

module.exports=PageContent;