var mongoose =require('./dbConnection');
var bcrypt = require('bcrypt');
var validator = require("email-validator");
var passwordValidator = require('password-validator');

var userSchema = new mongoose.Schema({  name:{type:String,unique:true,required:true,trim:true},
                    					email:{type:String,unique:true,required:true,trim:true},
                              password: {type:String,unique:true,required:true,trim:true} ,
                              isAdmin:Boolean,
                              isSubscriber:Boolean		
                					});

var passwordSchema = new passwordValidator();
 
// Add properties to it
passwordSchema
              .is().min(8)                                    // Minimum length 8
              .is().max(100)                                  // Maximum length 100
              .has().uppercase()                              // Must have uppercase letters
              .has().lowercase()                              // Must have lowercase letters
              .has().digits()                                 // Must have digits
              .has().not().spaces()                           // Should not have spaces
              .is().not().oneOf(['Passw0rd', 'Password123']); // Blacklist these values

//hashing a password before saving it to the database
userSchema.pre('save', function (next) {
  var user = this;
  /*enable when ready for production
  if(!validator.validate(user.email)){ 
    return next(new Error('Invalid email'));
  }
  if(!passwordSchema.validate(user.password)){
    console.log("INVALID PASSWORD")
    return next(new Error('Invalid password'));
  }
  */
  bcrypt.hash(user.password, 10, function (err, hashed){
    if (err) {
      return next(err);
    }
    user.password = hashed;
    next();
  })
});

var User = mongoose.model('User', userSchema);

module.exports=User;



