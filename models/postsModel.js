var mongoose =require('./dbConnection');

var postSchema = new mongoose.Schema({  title:String,
										shortbody:String,
										body: String,
										authorID:String,								
										dateofcreation:{hours:String, minutes:String, seconds:String, day:String, date:String, month:String,year:String},
										editdate:{hours:String, minutes:String, seconds:String, day:String, date:String, month:String,year:String},
										likes: Number,
										idofUsersWhoLiked:[String],
										image:Buffer,								
									});


var Post = mongoose.model('Post', postSchema);

module.exports=Post;