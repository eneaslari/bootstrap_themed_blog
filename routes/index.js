var express = require('express');
var router = express.Router();
var Post=require('../models/postsModel');
var NavBar=require('../models/navbarModel');
var PageContent=require('../models/pageContentModel');
var Subscribers=require('../models/subscriberModel');
var fs = require('fs');
var path=require('path');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

/* GET home page. */
router.get('/', function(req, res, next) {
	//epistrefei apo ola ta posts 5 kai apo ayta kanei skip ta 3. an thelo na epistrefo ta 5 xoris ta prota 5 kano prota skip
    var query=Post.find({}).limit(50).skip();
    query.exec(function (err, posts) {
    	for(var i=0;i<posts.length;i++){
			try{
				fs.writeFileSync('./public/'+""+i+ '.temp.png',posts[i].image);	
				posts[i].shortbody=cutString(150,posts[i].shortbody)				
				posts[i].imagename=''+i  + '.temp.png'						
			}catch(e){
				console.log(e);
			}     		
		} 
		var isAdmin=false
		if(req.session!=null){
			isAdmin=req.session.userAdmin	
		}
		NavBar.find({}, (req, elements) => {
			PageContent.find({ }, (err, contents) => {
				res.render('index', { posts:posts,isAdmin:isAdmin,elements:elements,contents: contents})
			});			
		});

	});	
});
function cutString(num,str){
	var newstr=""
	if(str && str.length>num){	
		for(var i=0;i<num;i++){
			newstr=newstr+(str.charAt(i));
		}
		//console.log(newstr);
		return newstr
	}
	return str
}


router.get("/editpagecontent", (req, res) => { 
    PageContent.find({ }, (err, contents) => {
        res.render('editpagecontent', { contents: contents})
    });
});

router.post("/savepagecontent", (req, res) => {
    console.log(req.body);
	PageContent.updateOne({ _id: req.body._id },{$set:{"content":req.body.content, "tag":req.body.tag,"page":req.body.page}}, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});

router.post("/addpagecontent", (req, res) => { 
	var pagecontentData = new PageContent(req.body);
	pagecontentData.save();
	res.redirect('/');

});

router.post("/subscribe", (req, res) => { 
	var subcriberdata = new Subscribers(req.body);
	subcriberdata.save();
	res.redirect('/');
});

module.exports = router;
