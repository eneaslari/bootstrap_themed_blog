var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var User=require('../models/usersModel');

/* GET users listing. */
router.get("/", (req, res) => { 
    User.find({}, (req, users) => {
			res.render('users', { users:users})
   	});
});

// Render registration page
router.get("/registration", (req, res) => {
  if(!req.session.userId){
    res.render('registration');
  }else{
    console.log("You are already registered");
    res.redirect('/');
  }    
});

//find the logged in user
router.get("/userloggedin", (req, res) => {
   User.findById(req.session.userId, (err, user) => {
   	  if(req.session.userId){
   	  	console.log(user.email);
   	  }else{
   	  	console.log("No user logged in");
   	  }
      res.redirect('/');
   });
});
//render login page
router.get("/login", (req, res) => {
	if(!req.session.userId){
    console.log("LETS LOGIN");
		res.render('login', {})
	}else{
		console.log("Unable(/login):Already login")
    res.redirect('/');
	}
});
//render edit profile page
router.get("/editprofile", (req, res) => {
   User.findById({_id:req.session.userId}, (err, user) => {
   		if(req.session.userId){
   			res.render('editprofile', {user:user})
   		}else{
				console.log("Unable(/editprofile):You are not logged in!");
				res.redirect('../users/login');
   		}
      	
   });
});

// GET /logout
router.get('/logout', function(req, res, next) {
  if (req.session.userId) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
    console.log("YOU LOGGED OUT")
  }
  else{
  	//something else
  	console.log("YOU ARE NOT LOGGED IN")
  	return res.redirect('/');
  }
});

router.post("/submitlogin", (req, res) => {
   User.findOne( {$or: [ {name:req.body.nameoremail} , {email:req.body.nameoremail} ]}, (err, user) => {
	   	if(user){
	   		bcrypt.compare(req.body.password, user.password, function (err, result) {
	        if (result === true) {
						req.session.userId = user._id;
						req.session.userAdmin=user.isAdmin;
	      		res.redirect('/');
	        } else {
	        	console.log("error:Wrong Password")
	   		    res.redirect('../users/login');
	        }
	      })
	   	}else{
	   		console.log("Error:username or email is wrong")
	   		res.redirect('../users/login');
	   	}     
   });

});

//submit user registration form
router.post('/createuser', (req, res) => {
	User.findOne( {$or: [ {name:req.body.name} , {email:req.body.email} ]}, (err, user) => {
	   	if(user){
	   		  console.log("This username or email already exists:"+" "+req.body.name+" "+req.body.email);
	      	res.redirect('/users/registration');
	   	}else{
				var user=req.body
				user.isAdmin=false;
				var userData = new User(user);
				console.log(user);
        userData.save().then( result => {
            res.redirect('/');
        }).catch(err => {
            console.log("invalid email or problem saving data");
            res.redirect('/users/registration');
            //res.status(400).send("Unable to save data!");
        });
				req.session.userId = userData._id;
				req.session.userAdmin=userData.isAdmin;
        console.log(req.session.userId);           
	   	}   
    });
});

router.post("/saveprofilechanges", (req, res) => {
	User.updateOne({ _id: req.session.userId },{$set:{"name":req.body.name, "email":req.body.email}}, function (err) {
  		if (err) return handleError(err);
	});

	res.redirect('/');
}); 

router.post("/changepassword", (req, res) => {
	User.findById({_id:req.session.userId}, (err, user) => {
		if(user){
			bcrypt.compare(req.body.oldpassword, user.password, function (err, result) {
					if (result === true) {
							if(req.body.newpassword==req.body.confirmpswd){
									user.password=req.body.newpassword;
									user.save(function (err,updateduser) {
											if (err) return handleError(err);
											console.log(updateduser);
									});
									res.redirect('/');
							}else{
								res.redirect('../users/editprofile');//or wherever the form
							}
							
					} else {
							console.log("error:Wrong old Password")
							res.redirect('../users/editprofile');
					}
			})
		}else{
					console.log("Error:Login First to change password")
					res.redirect('../users/editprofile');
		}     
	});
}); 

module.exports = router;
