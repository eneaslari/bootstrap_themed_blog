var express = require('express');
var router = express.Router();
var fs = require('fs');


var NavBar=require('../models/navbarModel');


router.get("/editnavbar", (req, res) => { 
    NavBar.find({ }, (err, elements) => {
        res.render('editnavbar', { elements: elements})
    });
});


router.post("/savenavbar", (req, res) => {
    console.log(req.body);
	NavBar.updateOne({ _id: req.body._id },{$set:{"element":req.body.element, "link":req.body.link,}}, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});

router.post("/addnavbar", (req, res) => { 
	var navbarData = new NavBar(req.body);
	navbarData.save();
	res.redirect('/');

});

module.exports = router;