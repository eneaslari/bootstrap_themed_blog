var express = require('express');
var router = express.Router();
var Post=require('../models/postsModel');
var formidable = require('formidable');
var fs = require('fs');

/* GET posts listing. */
router.get("/", (req, res) => { 
    Post.find({}, (req, posts) => {
		res.render('posts', { posts:posts})
   	});
});

router.get("/myposts", (req, res) => {
	if(req.session.userId){
	   	Post.find({authorID:req.session.userId}, (err, posts) => {
	    	for(var i=0;i<posts.length;i++){
				try{
					fs.writeFileSync('./public/'+""+i+ '.temp.png',posts[i].image);	
					posts[i].imagename='../'+i  + '.temp.png'											
				}catch(e){
					console.log(e);
				}     		
			} 
			var isAdmin=req.session.userAdmin 	
	    	res.render('index', { posts:posts,isAdmin:isAdmin}); 	
	    });
	}else{
		//you must login first
		res.redirect('../users/login');
	}

});

router.post('/addpost', (req, res) => {
	req.session.formdata="";
	req.session.filepath=""
	if(req.session.userId){
		var form = new formidable.IncomingForm();
		form.parse(req,function(err,fields,files){
			if(err) next(err);
			fields.authorID=req.session.userId;
			fields.dateofcreation=todayFormated();
			fields.editdate=todayFormated();
			req.session.formdata=(fields);			
		});
		//when a file have been recieved
		form.on('fileBegin', function(name, file) {
			file.path=name+'.temp.png'
			req.session.filepath=file.path
		});
	    form.on('end', function (){
			var imageData = fs.readFileSync(req.session.filepath);
			req.session.formdata.image=imageData;	
			// Create an Image instance
			var postData = new Post(req.session.formdata);
			postData.save();
			delete req.session.formdata
			res.redirect('/');
	    });
	}else{
		//you must login first
		res.redirect('../users/login');
	}

});

router.post("/deletepost", (req, res) => {
	console.log(req.body._id);
	Post.deleteOne({ _id: req.body._id }, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});

router.post("/editpost", (req, res) => {
   Post.find({ _id: req.body._id }, (err, posts) => {
      res.render('editpage', { posts: posts})
   });
});

router.post("/editsave", (req, res) => {
	Post.updateOne({ _id: req.body._id },{$set:{"title":req.body.title, "shortbody":req.body.shortbody,"body":req.body.body}}, function (err) {
  		if (err) return handleError(err);
	});
	res.redirect('/');
});

//help functions
function todayFormated(){
	var today = new Date();
	var hr = today.getHours();
	var min = today.getMinutes();
	var sec = today.getSeconds();
	var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
	var day = days[today.getDay()];
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10){
	    dd='0'+dd;
	} 
	if(mm<10){
	    mm='0'+mm;
	} 
	//var today = hr+':'+min+':'+sec+'-'+day+' '+dd+'/'+mm+'/'+yyyy;
	var today={hours:hr,minutes:min,seconds:sec,day:day,date:dd,month:mm,year:yyyy}
	return today;
}

module.exports = router;